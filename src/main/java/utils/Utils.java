package utils;

import driver.DriverManager;
import org.openqa.selenium.*;

import static logger.AllureLogger.*;

public class Utils {

    public static void goToPageURL(final String url) {
        logToAllureInfo("Going to URL: " + url);
        DriverManager.getDriver().get(url);
    }

    public static String getLocatorFromElement(WebElement element) {
        try {
            return element.toString()
                    .split("->")[1]
                    .replaceFirst("(?s)(.*)]", "$1" + "");
        } catch (ArrayIndexOutOfBoundsException e) {
            logToAllureError(String.valueOf(e.getCause()));
            return "parsing of locator from element failed !!";
        }
    }
}