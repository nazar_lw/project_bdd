package businessobject;

import pageobject.*;

public class LoginationBO {

    private LoginPage loginPage;

    private HomePage homePage;

    public LoginationBO() {
        loginPage = new LoginPage();
        homePage = new HomePage();
    }

    public void logIn(String email, String password) {
        loginPage.typeEmailAndSubmit(email);
        loginPage.typePasswordAndSubmit(password);
    }

    public boolean areAccountOptionsPresent() {
        return homePage.areAccountOptionsPresent();
    }

    public void logOut() {
        homePage.logOut();
    }
}
