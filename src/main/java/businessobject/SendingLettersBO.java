package businessobject;

import dto.MessageDTO;
import dto.StateOfLettersPageDTO;
import pageobject.*;

public class SendingLettersBO {

    private HandleLettersPage handleLettersPage;

    private SingleLetterPage singleLetterPage;

    private SentLettersPage sentLettersPage;

    public SendingLettersBO() {
        handleLettersPage = new HandleLettersPage();
        sentLettersPage = new SentLettersPage();
        singleLetterPage = new SingleLetterPage();
    }

    public void sendNewLetter(MessageDTO messageDTO) {
        handleLettersPage.openCreateLetterForm();
        singleLetterPage.fillLetter(messageDTO);
        singleLetterPage.sendLetter();
    }

    public String getLastLetterSubject() {
        handleLettersPage.getAllSentLettersPage();
        return sentLettersPage.getLastLetterSubject();
    }

    public StateOfLettersPageDTO getStateOfTheLettersList(String tagAttribute) {
        StateOfLettersPageDTO stateOfLettersPageDTO = new StateOfLettersPageDTO();
        stateOfLettersPageDTO.setSizeOfLettersList(sentLettersPage.getSizeOfLettersList());
        stateOfLettersPageDTO.setRecipientEmailOfTheLastLetter(sentLettersPage.getRecipientEmailOfTheLastLetter(tagAttribute));
        return stateOfLettersPageDTO;
    }
}
