package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

import driver.DriverManager;
import org.testng.annotations.*;
import listener.CustomListeners;


@Listeners({CustomListeners.class})
@CucumberOptions(
        plugin = {"pretty", "json:src/test/java/reporting/cucumber2.json"},
        strict = true,
        features = {"src/test/resources"},
        glue = {"steps"}
)
public class GmailTestRunner extends AbstractTestNGCucumberTests {

    @AfterMethod
    public void afterMethod() {
        DriverManager.quitDriver();
    }

}
