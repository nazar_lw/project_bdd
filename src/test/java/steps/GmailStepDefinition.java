package steps;

import businessobject.DeletingLettersBO;
import businessobject.LoginationBO;
import businessobject.SendingLettersBO;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dto.MessageDTO;
import dto.StateOfLettersPageDTO;
import listener.CustomListeners;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import smtpservice.MailSender;

import static utils.PropertyFileHandler.ATTRIBUTE_EMAIL;
import static utils.PropertyFileHandler.MAIN_URL;
import static utils.Utils.goToPageURL;

public class GmailStepDefinition {

    private LoginationBO loginationBO;
    private SendingLettersBO sendingLettersBO;
    private DeletingLettersBO deletingLettersBO;
    private StateOfLettersPageDTO dtoAfterSending;
    private StateOfLettersPageDTO dtoAfterDeleting;

    public GmailStepDefinition(){
        loginationBO = new LoginationBO();
        sendingLettersBO = new SendingLettersBO();
        deletingLettersBO = new DeletingLettersBO();
    }

    @Given("^I go to gmail$")
    public void iGoToGmail() throws Throwable {
        goToPageURL(MAIN_URL);
    }

    @And("^I send default letter from \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iSendDefaultLetter(String email, String password) throws Throwable {
        new MailSender().sendDefaultMessage(email, password);
    }

    @When("^I logIn with credentials \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iLogInWithCredentials(String email, String password) throws Throwable {
        loginationBO.logIn(email, password);
    }

    @Then("^I should check if I logged in$")
    public void iShouldCheckIfILoggedIn() throws Throwable {
        Assert.assertTrue(loginationBO.areAccountOptionsPresent(), " Logination failed");
    }

    @When("^I send a letter$")
    public void iSendALetter() throws Throwable {
        MessageDTO messageDTO = new MessageDTO();
        sendingLettersBO.sendNewLetter(messageDTO);
    }

    @Then("^I should check if the subject \"([^\"]*)\" is \"([^\"]*)\"$")
    public void iShouldCheckIfTheSubjectMatches(String messageSubject, String isEqual) throws Throwable {
        boolean assertion = false;
        if (isEqual.equals("equal")){
            assertion = true;
        }
        Assert.assertEquals(sendingLettersBO.getLastLetterSubject().equals(messageSubject),
                assertion, " Subject of the letter does not match !");
    }

    @And("^I should get the state of the page of sent letters after sending$")
    public void iShouldGetTheStateOfThePageOfSentLettersAfterSending() throws Throwable {
        dtoAfterSending = sendingLettersBO.getStateOfTheLettersList(ATTRIBUTE_EMAIL);
    }

    @When("^I delete the last letter in the list$")
    public void iDeleteTheLastLetterInTheList() throws Throwable {
        deletingLettersBO.deleteLastLetter();
    }

    @Then("^I should get the state of the page of sent letters after deleting$")
    public void iShouldGetTheStateOfThePageOfSentLettersAfterDeleting() throws Throwable {
        dtoAfterDeleting = sendingLettersBO.getStateOfTheLettersList(ATTRIBUTE_EMAIL);
    }

    @And("^I should check if states of the page before and after deleting has changed$")
    public void iShouldCheckIfStatesOfThePageBeforeAndAfterDeletingHasChanged() throws Throwable {
        Assert.assertNotEquals(dtoAfterSending.getSizeOfLettersList(), dtoAfterDeleting.getSizeOfLettersList(),
                " The size of the sent letters page has not changed after deleting !");
        Assert.assertNotEquals(dtoAfterSending.getRecipientEmailOfTheLastLetter(), dtoAfterDeleting.getRecipientEmailOfTheLastLetter(),
                " The recipientEmail of the last letter has not changed after deleting  !");
    }

    @And("^I should clean up the page$")
    public void iShouldCleanUpThePage() throws Throwable {
        deletingLettersBO.cleanUpMailBox();
    }
}
