Feature: GmailTest

  Scenario Outline: Verify Sending and Deleting of a letter
    Given I go to gmail
    And  I send default letter from "<email>" and "<password>"
    When I logIn with credentials "<email>" and "<password>"
    Then I should check if I logged in
    When I send a letter
    Then I should check if the subject "<messageSubject>" is "<isEqual>"
    And  I should get the state of the page of sent letters after sending
    When I delete the last letter in the list
    Then I should get the state of the page of sent letters after deleting
    And  I should check if states of the page before and after deleting has changed
    And  I should clean up the page


    Examples:

    | email                    | password             | messageSubject   | isEqual  |
    | testepamivan@gmail.com   | myprojectmailsende@1 | Test subject     | equal    |
    | testepampetro@gmail.com  | myprojectmailsende@2 | Other subject    | notEqual |
    | test1pavlo@gmail.com     | myprojectmailsende@3 | Test subject     | equal    |
    | dimamalash5@gmail.com    | myprojectmailsende@4 | Other subject    | notEqual |
    | gavruloepam@gmail.com    | myprojectmailsende@5 | Test subject     | equal    |
